﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {

        public Page2()
        {
            InitializeComponent();
        }

        // Correct Button was pressed, moves to next page
        public async void ImageButton_Clicked(object sender, System.EventArgs e)
        {

            await Navigation.PushAsync(new Page3());

        
        }

        // Clicked wrong image, go back one page
        public async void ImageButton_Clicked_1(object sender, System.EventArgs e)
        {
            // Saves answer from aler
            var answer = await DisplayAlert("Question?", "Are you sure?", "Yes", "No");

            // Answer is true - Then wrong answer
            if (answer == true)
            {
                await Navigation.PushAsync(new Page4());
            }
            
            else
            {
                // Do nothing 
                return;
            }

            
        }

    }
}