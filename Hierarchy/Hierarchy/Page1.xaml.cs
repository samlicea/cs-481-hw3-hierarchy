﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }

        // Pushes page 2 into stack, automatically handles navigating back to previous page
        private void StartGame(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Page2());
        }

        // Pushes user to Give Up Page (Page 4)
        private void GiveUp(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Page4());
        }

        private void Secretbutton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Page5());
        }
    }
}